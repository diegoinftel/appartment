package com.inftel.appartment;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.inftel.appartment.asynctasks.CrearPisoAsyncTask;

public class NoPisoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_no_piso);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.no_piso, menu);
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_new:
			raiseDialog();
			return true;
		case R.id.action_settings:
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void raiseDialog() {
		final Dialog dialog = new Dialog(this);
		final EditText nombre = (EditText) dialog.findViewById(R.id.nuevo_piso);
		final EditText direccion = (EditText) dialog.findViewById(R.id.direccion);
		Button b = (Button) dialog.findViewById(R.id.dialogButtonOK);
		dialog.setContentView(R.layout.dialogo_add_piso);
		dialog.setTitle("Nuevo piso");

		b.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new CrearPisoAsyncTask(NoPisoActivity.this).execute(nombre.getText().toString(), direccion.getText().toString());
				dialog.dismiss();
			}
		});
		Button bCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
		bCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});
		dialog.show();
	}
}