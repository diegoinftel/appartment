package com.inftel.appartment.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Utilities {

	public static String inputStreamToString(InputStream in) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		StringBuilder out = new StringBuilder();
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return out.toString();
	}

	public static String getResponseText(InputStream inStream) {

		return new Scanner(inStream).useDelimiter("\\A").next();
	}
}