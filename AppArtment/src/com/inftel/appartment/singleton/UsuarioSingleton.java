package com.inftel.appartment.singleton;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.inftel.appartment.model.Piso;
import com.inftel.appartment.model.Usuario;

public class UsuarioSingleton {

	
	private static UsuarioSingleton INSTANCE = null;
	
	private Usuario u;
	private Piso p;
	private List<Usuario> roommates;
	
    private UsuarioSingleton(){
    	u = new Usuario();
    	roommates=new ArrayList<Usuario>();
    }
 

    private synchronized static void createInstance() {
        if (INSTANCE == null) { 
            INSTANCE = new UsuarioSingleton();
        }
    }

    public static UsuarioSingleton getInstance() {
        createInstance();
        return INSTANCE;
    }
    
    public void init(JSONObject usuario){
    	try {
			this.u.setEmail(usuario.getString("email"));
			this.u.setNombre(usuario.getString("nombre"));
			this.u.setId(Short.parseShort(usuario.getString("id")));
			this.u.setToken(usuario.getString("token"));
			if (usuario.has("idPiso")){
				this.p = new Piso();
				JSONObject piso = new JSONObject(usuario.getString("idPiso"));
				this.p.setDireccion(piso.getString("direccion"));
				this.p.setNombre(piso.getString("nombre"));
				this.p.setId(Short.parseShort(piso.getString("id")));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
    }
    
    public void init(JSONArray roommates){
    	try {
    		this.roommates=new ArrayList<Usuario>();
    		for(int i = 0; i < roommates.length(); i++){
    			JSONObject b=roommates.getJSONObject(i);
    			Usuario u=new Usuario();
				u.setEmail(b.getString("email"));
				u.setNombre(b.getString("nombre"));
				u.setId(Short.parseShort(b.getString("id")));
				u.setToken(b.getString("token"));
				this.roommates.add(u);
    		}
		} catch (JSONException e) {
			e.printStackTrace();
		}
    }
    
    public void setPiso (Piso p){
    	this.p = p;
    }
    
    public Piso getPiso (){
    	return this.p;
    }
    
    public void setUsuario (Usuario u){
    	this.u = u;
    }
    
    public Usuario getUsuario () {
    	return this.u;
    }


	public List<Usuario> getRoommates() {
		return roommates;
	}


	public void setRoommates(List<Usuario> roommates) {
		this.roommates = roommates;
	}
    
    
}
