package com.inftel.appartment.asynctasks;


import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.inftel.appartment.AddFacturaActivity;
import com.inftel.appartment.conexiones.InsertarFacturasConnection;
import com.inftel.appartment.singleton.UsuarioSingleton;


public class InsertarFacturasAsyncTask extends AsyncTask<JSONObject,Integer,JSONObject> {
	ProgressDialog dialog;
	AddFacturaActivity c;

	public InsertarFacturasAsyncTask (AddFacturaActivity c){
		this.c = c;
		dialog = new ProgressDialog(c);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Almacenando factura...");
		dialog.setIndeterminate(true);
		dialog.show();
	}

	@Override
	protected JSONObject doInBackground(JSONObject... dato) {
		return InsertarFacturasConnection.InsertarFacturas(dato[0]); 
	}

	protected void onPostExecute(JSONObject result){
		super.onPostExecute(result);
		try{
			UsuarioSingleton u= UsuarioSingleton.getInstance();
			JSONObject usuarioPorFactura;
			JSONObject idUsuario;
			JSONObject idPiso;

			for(int i=0;i<u.getRoommates().size();i++){
				InsertarFacturaPorUsuarioAsyncTask tarea2= new InsertarFacturaPorUsuarioAsyncTask();
				usuarioPorFactura = new JSONObject();
				idPiso = new JSONObject();
				idUsuario = new JSONObject();

				usuarioPorFactura.put("idFactura", result);

				idPiso.put("id", u.getPiso().getId());

				usuarioPorFactura.put("idPiso", idPiso);

				idUsuario.put("id", u.getRoommates().get(i).getId());

				usuarioPorFactura.put("idUsuario", idUsuario);
				usuarioPorFactura.put("ispagada", 0);

				tarea2.execute(usuarioPorFactura);

			}
			InsertarFacturaPorUsuarioAsyncTask tarea3 = new InsertarFacturaPorUsuarioAsyncTask();
			usuarioPorFactura=new JSONObject();
			idUsuario = new JSONObject();
			idPiso = new JSONObject();
			usuarioPorFactura.put("idFactura", result);
			idPiso.put("id", u.getPiso().getId());
			usuarioPorFactura.put("idPiso", idPiso);
			idUsuario.put("id", u.getUsuario().getId());
			usuarioPorFactura.put("idUsuario", idUsuario);
			usuarioPorFactura.put("ispagada", 0);

			tarea3.execute(usuarioPorFactura);

		}
		catch (JSONException e){
			e.printStackTrace();
		}
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		Toast.makeText(c, "Factura creada con exito", Toast.LENGTH_SHORT).show();
		c.finish();
	}
}
