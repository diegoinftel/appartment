package com.inftel.appartment.asynctasks;

import org.json.JSONArray;

import android.os.AsyncTask;

import com.inftel.appartment.conexiones.RoommatesConnection;


public class RoommatesAsyncTask extends AsyncTask<String,Integer,JSONArray> {
	
	@Override
	protected JSONArray doInBackground(String... params) {
		return RoommatesConnection.obtenerRoommates(params[0]); 
	}
}
