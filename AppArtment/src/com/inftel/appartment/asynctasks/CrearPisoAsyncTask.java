package com.inftel.appartment.asynctasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.inftel.appartment.FacturasActivity;
import com.inftel.appartment.NoPisoActivity;
import com.inftel.appartment.conexiones.CrearPisoConnection;
import com.inftel.appartment.model.Piso;
import com.inftel.appartment.singleton.UsuarioSingleton;

public class CrearPisoAsyncTask extends AsyncTask<String, Void, JSONObject> {

	ProgressDialog dialog;
	NoPisoActivity c;

	public CrearPisoAsyncTask (NoPisoActivity c){
		this.c = c;
		dialog = new ProgressDialog(c);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Almacenando factura...");
		dialog.setIndeterminate(true);
		dialog.show();
	}

	@Override
	protected JSONObject doInBackground(String... arg0) {
		return CrearPisoConnection.crearPiso(arg0[0], arg0[1]);
	}

	protected void onPostExecute(JSONObject result){
		super.onPostExecute(result);
		UsuarioSingleton us = UsuarioSingleton.getInstance();
		Piso p = new Piso();
		try {
			p.setNombre(result.getString("nombre"));
			p.setDireccion(result.getString("direccion"));
			p.setId(Short.parseShort(result.getString("id")));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		us.setPiso(p);
		Toast.makeText(c, "Piso creado con exito", Toast.LENGTH_SHORT).show();
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		Intent i = new Intent(c, FacturasActivity.class);
		c.startActivity(i);
	}
}
