package com.inftel.appartment.asynctasks;

import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.inftel.appartment.MostrarDetallesFacturaActivity;
import com.inftel.appartment.conexiones.PagarFacturaConnection;

public class PagarFacturaAsyncTask extends AsyncTask<JSONObject, Integer, JSONArray> {
	
	ProgressDialog dialog;
	MostrarDetallesFacturaActivity c;
	JSONObject factura;

	public PagarFacturaAsyncTask (MostrarDetallesFacturaActivity c){
		this.c = c;
		dialog = new ProgressDialog(c);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Marcando como pagada...");
		dialog.setIndeterminate(true);
		dialog.show();
	}

	@Override
	protected JSONArray doInBackground(JSONObject... params) {
		factura = params[1];
		return PagarFacturaConnection.pagarFactura(params[0]);	
	}

	@Override
	protected void onPostExecute(JSONArray result){
		super.onPostExecute(result);
		if(result.length()==0){
			ActualizarFacturaAsyncTask tarea2=new ActualizarFacturaAsyncTask();
			try {
				factura.put("pagada", 1);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			try {
				tarea2.execute(factura).get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		c.finish();
	}
}
