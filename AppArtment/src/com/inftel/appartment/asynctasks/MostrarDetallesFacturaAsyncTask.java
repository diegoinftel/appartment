package com.inftel.appartment.asynctasks;

import org.json.JSONArray;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.inftel.appartment.MostrarDetallesFacturaActivity;
import com.inftel.appartment.conexiones.MostarDetallesFacturaConnection;

public class MostrarDetallesFacturaAsyncTask extends AsyncTask<String, Void, JSONArray> {

	ProgressDialog dialog;
	MostrarDetallesFacturaActivity c;

	public MostrarDetallesFacturaAsyncTask (MostrarDetallesFacturaActivity c){
		this.c = c;
		dialog = new ProgressDialog(c);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Cargando factura...");
		dialog.setIndeterminate(true);
		dialog.show();
	}

	@Override
	protected JSONArray doInBackground(String... params) {
		return MostarDetallesFacturaConnection.ObtenerFacturas(params[0]);	
	}
	
	@Override
	protected void onPostExecute(JSONArray result){
		super.onPostExecute(result);
		c.mostarDatos(result);
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
	}
}
