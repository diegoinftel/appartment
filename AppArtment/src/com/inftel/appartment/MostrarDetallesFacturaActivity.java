package com.inftel.appartment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.inftel.appartment.asynctasks.MostrarDetallesFacturaAsyncTask;
import com.inftel.appartment.asynctasks.PagarFacturaAsyncTask;
import com.inftel.appartment.singleton.UsuarioSingleton;

public class MostrarDetallesFacturaActivity extends Activity {

	ListView lstUsuarios;
	TextView txtConcepto ;
	TextView txtCantidad ;
	TextView txtNotas ;
	TextView txtFecha;
	Button btnPagar;
	JSONObject factura;
	int idusuarioporfactura;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mostrar_factura);		
		String facturaID= String.valueOf(getIntent().getExtras().getInt("idFactura"));
		MostrarDetallesFacturaAsyncTask tarea=new MostrarDetallesFacturaAsyncTask(MostrarDetallesFacturaActivity.this);

		lstUsuarios = (ListView) findViewById(R.id.lstUsuarios);
		txtConcepto = (TextView) findViewById(R.id.txtConcepto);
		txtCantidad = (TextView) findViewById(R.id.txtCantidad);
		txtNotas = (TextView) findViewById(R.id.txtNotas);
		txtFecha = (TextView) findViewById(R.id.txtFecha);
		btnPagar=(Button) findViewById(R.id.btnPagar);

		btnPagar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				pagar();
			}
		});

		tarea.execute(facturaID);
	}



	public void mostarDatos(JSONArray datos){

		String[] usuarios = new String[datos.length()];
		JSONObject usuarioPorFactura=null;
		UsuarioSingleton u=UsuarioSingleton.getInstance();
		String esPagado;
		try {
			usuarioPorFactura = datos.getJSONObject(0);
			factura = usuarioPorFactura.getJSONObject("idFactura");
			String fechaFac = factura.getString("fecha");
			txtFecha.setText(fechaFac.substring(0, 10));
			String notasFac = new String();
			if (factura.has("notas"))
				notasFac = factura.getString("notas");
			txtNotas.setText(notasFac);
			String conceptoFac = factura.getString("concepto");
			txtConcepto.setText(conceptoFac);
			int cantidadFac = factura.getInt("cantidad");	
			txtCantidad.setText(String.valueOf(cantidadFac));

		} catch (JSONException e1) {
			e1.printStackTrace();
		}	


		for (int i = 0; i < datos.length(); i++) {			
			try {
				usuarioPorFactura = datos.getJSONObject(i);
				JSONObject usuarioJSON = usuarioPorFactura.getJSONObject("idUsuario");
				String nombreUsuario = usuarioJSON.getString("nombre");
				int pagado = usuarioPorFactura.getInt("ispagada");
				if(u.getUsuario().getId() == usuarioJSON.getInt("id"))
				{
					idusuarioporfactura = usuarioPorFactura.getInt("id");
				}


				if(pagado==1)
					esPagado="La factura ha sido pagada";
				else
					esPagado="La factura no ha sido pagada";

				usuarios[i] = "" + nombreUsuario + "-" + esPagado;

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		ArrayAdapter<String> adaptador = new ArrayAdapter<String>(
				MostrarDetallesFacturaActivity.this,
				android.R.layout.simple_list_item_1, usuarios);

		lstUsuarios.setAdapter(adaptador);
	}

	private void pagar(){

		JSONObject pagado=new JSONObject();
		JSONObject idUsuario=new JSONObject();
		JSONObject idPiso=new JSONObject();
		JSONObject idFactura=new JSONObject();
		UsuarioSingleton u=UsuarioSingleton.getInstance();
		try {
			idUsuario.put("id", u.getUsuario().getId());
			idPiso.put("id", u.getPiso().getId());

			idFactura.put("id", factura.getInt("id"));
			pagado.put("idUsuario", idUsuario);
			pagado.put("idPiso", idPiso);

			pagado.put("id", idusuarioporfactura);

			pagado.put("idFactura", idFactura );
			pagado.put("ispagada", 1);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		PagarFacturaAsyncTask tarea=new PagarFacturaAsyncTask(MostrarDetallesFacturaActivity.this);
		tarea.execute(pagado, factura);
	}

}