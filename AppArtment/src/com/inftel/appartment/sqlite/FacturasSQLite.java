package com.inftel.appartment.sqlite;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FacturasSQLite {
	public static final String KEY_ROWID = "_id";
	public static final String KEY_ID_FACTURA_EN_BD = "id_real_bd";
	public static final String KEY_CONCEPTO = "concepto_factura";
	public static final String KEY_CANTIDAD = "cantidad_factura";
	public static final String KEY_FECHA = "fecha_factura";
	public static final String KEY_NOTAS = "notas_factura";
	public static final String KEY_PAGADA = "pagada_total";
	public static final String KEY_IDPISO = "idpiso_factura";
	public static final String KEY_IS_PAGADA = "is_pagada";

	public static final String DATABASE_NAME = "FacturasSQLite";
	public static final String DATABASE_TABLE = "factura";
	public static final int DATABASE_VERSION = 1;

	private static final String SQL_CREATE_ENTRIES = "CREATE TABLE "
			+ DATABASE_TABLE + " (" + KEY_ROWID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_ID_FACTURA_EN_BD
			+ " INTEGER NOT NULL, " +

			KEY_CONCEPTO + " TEXT NOT NULL, " + KEY_CANTIDAD
			+ " REAL NOT NULL, " + KEY_FECHA + " TEXT NOT NULL, " + KEY_NOTAS
			+ " TEXT NOT NULL, " + KEY_PAGADA + " INTEGER NOT NULL, "
			+ KEY_IDPISO + " INTEGER NOT NULL, " +

			KEY_IS_PAGADA + " INTEGER NOT NULL);";

	private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
			+ DATABASE_TABLE;

	private final Context ourContext;
	private SQLiteDatabase ourDatabase;
	private FeedReaderDbHelper ourHelper;

	public class FeedReaderDbHelper extends SQLiteOpenHelper {

		public FeedReaderDbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(SQL_CREATE_ENTRIES);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

			db.execSQL(SQL_DELETE_ENTRIES);
			onCreate(db);
		}

		public void borrar(SQLiteDatabase db) {
			db.execSQL(SQL_DELETE_ENTRIES);
			onCreate(db);
		}

	}

	public FacturasSQLite(Context c) {
		ourContext = c;
	}

	public FacturasSQLite open() throws SQLException {
		ourHelper = new FeedReaderDbHelper(ourContext);
		ourDatabase = ourHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		ourHelper.close();
	}

	public long insertarFactura(int idReal, String concepto, float cantidad,
			String fecha, String notas, int pagadaTotalmente, int id_piso,
			int is_pagada) {
		ContentValues values = new ContentValues();
		values.put(KEY_ID_FACTURA_EN_BD, idReal);
		values.put(KEY_CONCEPTO, concepto);
		values.put(KEY_CANTIDAD, cantidad);
		values.put(KEY_FECHA, fecha);
		values.put(KEY_NOTAS, notas);
		values.put(KEY_PAGADA, pagadaTotalmente);
		values.put(KEY_IDPISO, id_piso);
		values.put(KEY_IS_PAGADA, is_pagada);

		return ourDatabase.insert(DATABASE_TABLE, null, values);
	}

	public ArrayList<String> leerDatos() {
		String[] columns = new String[] { KEY_ROWID, KEY_CONCEPTO,
				KEY_CANTIDAD, KEY_FECHA, KEY_NOTAS, KEY_PAGADA, KEY_IDPISO,
				KEY_IS_PAGADA };
		Cursor c = ourDatabase.query(DATABASE_TABLE, columns, null, null, null,
				null, null);
		String fac = "";
		ArrayList<String> listaFacturas = new ArrayList<String>();

		int iConcepto = c.getColumnIndex(KEY_CONCEPTO);
		int iCantidad = c.getColumnIndex(KEY_CANTIDAD);
		int iFecha = c.getColumnIndex(KEY_FECHA);

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			fac = c.getString(iConcepto) + " " + c.getString(iCantidad) + " "
					+ c.getString(iFecha) + "\n";
			listaFacturas.add(fac);
			fac = "";
		}

		return listaFacturas;
	}

	public void delete() {
		ourDatabase.delete(DATABASE_TABLE, null, null);
	}
}
