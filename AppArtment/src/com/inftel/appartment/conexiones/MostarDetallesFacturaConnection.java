package com.inftel.appartment.conexiones;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import com.inftel.appartment.singleton.UsuarioSingleton;
import com.inftel.appartment.utilities.StaticResources;

public class MostarDetallesFacturaConnection {

	private static String IP = StaticResources.ip;
	private static String PUERTO = StaticResources.puerto;
	private static String URL_OBTENER_FACTURA = StaticResources.url_obtener_facturas_por_usuario;

	private static String URL_TOKEN = StaticResources.url_token;

	public static JSONArray ObtenerFacturas(String idFactura) {

		UsuarioSingleton u = UsuarioSingleton.getInstance();

		HttpClient httpClient = new DefaultHttpClient();

		HttpGet del = new HttpGet(IP + ":" + PUERTO + URL_OBTENER_FACTURA
				+ idFactura + URL_TOKEN + u.getUsuario().getToken());

		del.setHeader("content-type", "application/json");
		JSONArray respJSON = null;

		HttpResponse resp;
		try {
			resp = httpClient.execute(del);
			String respStr = EntityUtils.toString(resp.getEntity());

			respJSON = new JSONArray(respStr);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return respJSON;
	}
}