package com.inftel.appartment.conexiones;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.inftel.appartment.singleton.UsuarioSingleton;
import com.inftel.appartment.utilities.StaticResources;

public class ActualizarFacturasConnection {

	private static String IP = StaticResources.ip;
	private static String PUERTO = StaticResources.puerto;
	private static String URL_ACTUALIZAR_FACTURA = StaticResources.url_actualizar_factura;

	public static JSONObject InsertarFacturas(JSONObject dato) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpPut put = new HttpPut(IP+":"+PUERTO+URL_ACTUALIZAR_FACTURA);
		UsuarioSingleton u=UsuarioSingleton.getInstance();
		put.addHeader("token", u.getUsuario().getToken());
		put.setHeader("content-type", "application/json");
		StringEntity entity;
		try {
			entity = new StringEntity(dato.toString());
			put.setEntity(entity);
			httpClient.execute(put);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
