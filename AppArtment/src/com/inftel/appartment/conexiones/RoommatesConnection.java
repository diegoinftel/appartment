package com.inftel.appartment.conexiones;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import com.inftel.appartment.singleton.UsuarioSingleton;
import com.inftel.appartment.utilities.StaticResources;

public class RoommatesConnection {

	private static String IP = StaticResources.ip;
	private static String PUERTO = StaticResources.puerto;
	private static String URL_ROOMMATES = StaticResources.url_roommates;
	private static String URL_TOKEN = StaticResources.url_token;
	private static String URL_PISO = StaticResources.url_piso;

	public static JSONArray obtenerRoommates(String email) {
		HttpClient httpClient = new DefaultHttpClient();
		UsuarioSingleton u = UsuarioSingleton.getInstance();
		HttpGet del = new HttpGet(IP + ":" + PUERTO + URL_ROOMMATES + email
				+ URL_TOKEN + u.getUsuario().getToken() + URL_PISO
				+ u.getPiso().getId());

		del.setHeader("content-type", "application/json");

		try {
			HttpResponse resp = httpClient.execute(del);
			String respStr = EntityUtils.toString(resp.getEntity());

			JSONArray respJSON = new JSONArray(respStr);
			return respJSON;

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;

	}
}
