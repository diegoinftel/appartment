package com.inftel.appartment;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.inftel.appartment.singleton.UsuarioSingleton;

public class MapActivity extends Activity {

	private GoogleMap map;
	private MarkerOptions markerOptions;
	private LatLng latLng;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		UsuarioSingleton us = UsuarioSingleton.getInstance();
		String location = us.getPiso().getDireccion();
		String nombre  = us.getPiso().getNombre();
		getActionBar().setDisplayHomeAsUpEnabled(true);
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		new GeocoderTask().execute(location, nombre);
	}

	private class GeocoderTask extends AsyncTask<String, Void, List<Address>>{

		private String nombre;

		@Override
		protected List<Address> doInBackground(String... locationName) {
			Geocoder geocoder = new Geocoder(getBaseContext());
			List<Address> addresses = null;
			nombre = locationName[1];
			try {
				addresses = geocoder.getFromLocationName(locationName[0], 3);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return addresses;
		}

		@Override
		protected void onPostExecute(List<Address> addresses) {

			if(addresses==null || addresses.size()==0){
				Toast.makeText(getBaseContext(), "No se ha encontrado esa direccion", Toast.LENGTH_SHORT).show();
			}

			map.clear();

			for(int i=0;i<addresses.size();i++){

				Address address = (Address) addresses.get(i);
				latLng = new LatLng(address.getLatitude(), address.getLongitude());
				markerOptions = new MarkerOptions();
				markerOptions.position(latLng);
				markerOptions.title(nombre);

				map.addMarker(markerOptions).showInfoWindow();

				if(i==0)
					map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,14.0f));
			}
		}
	}
}