package com.inftel.appartment;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.inftel.appartment.asynctasks.RegistrarUsuarioAsyncTask;

public class LoginActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener{

	private static final int RC_SIGN_IN = 0;

	private GoogleApiClient mGoogleApiClient;

	private boolean mIntentInProgress;

	private String email;

	private String nombre;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		SignInButton b = (SignInButton) findViewById(R.id.sign_in_button);
		
		getActionBar().hide();

		mGoogleApiClient = new GoogleApiClient.Builder(this)
		.addConnectionCallbacks(this)
		.addOnConnectionFailedListener(this)
		.addApi(Plus.API, null)
		.addScope(Plus.SCOPE_PLUS_LOGIN)
		.build();

		b.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new RegistrarUsuarioAsyncTask(LoginActivity.this).execute(email, nombre);
			}
		});
	}

	//<<<----- CONEXION CON LOS SERVIDORES DE GOOGLE PARA REALIZAR LA AUTENTICACION ----->>>

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!mIntentInProgress && result.hasResolution()) {
			try {
				mIntentInProgress = true;
				result.startResolutionForResult(this, RC_SIGN_IN);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		this.email = Plus.AccountApi.getAccountName(mGoogleApiClient);
		this.nombre = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient).getDisplayName();
		nombre = nombre.split("\\s+")[0];
	}

	@Override
	public void onConnectionSuspended(int cause) {
		mGoogleApiClient.connect();
	}

	@Override
	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
		if (requestCode == RC_SIGN_IN) {
			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}

		}
	}
}
