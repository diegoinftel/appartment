package com.inftel.appartment;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.inftel.appartment.asynctasks.InvitarUsuarioAsyncTask;
import com.inftel.appartment.asynctasks.ObtenerFacturasAsyncTask;
import com.inftel.appartment.singleton.UsuarioSingleton;
import com.inftel.appartment.sqlite.FacturasSQLite;

public class FacturasActivity extends Activity {


	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_facturas);
		ObtenerFacturasAsyncTask tarea = new ObtenerFacturasAsyncTask(FacturasActivity.this);
		tarea.execute();
	}

	@Override
	protected void onResume(){
		super.onResume();
		ObtenerFacturasAsyncTask tarea = new ObtenerFacturasAsyncTask(FacturasActivity.this);
		tarea.execute();
	}


	public void leer(View view) {
		Intent intent = new Intent(FacturasActivity.this, FacturasEnBD.class);
		startActivity(intent);
	}

	public void limpiarBD(View view) {
		FacturasSQLite entry = new FacturasSQLite(FacturasActivity.this);
		entry.open();
		entry.delete();
		entry.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.facturas_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add_factura:
			Intent add = new Intent(getApplicationContext(),
					AddFacturaActivity.class);
			startActivity(add);
			return true;
		case R.id.action_map:
			Intent map = new Intent(getApplicationContext(), MapActivity.class);
			startActivity(map);
			return true;
		case R.id.action_invitar:
			invitar();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void invitar() {

		final Dialog dialog = new Dialog(this);
		final EditText email = (EditText) dialog.findViewById(R.id.email);
		dialog.setContentView(R.layout.dialog_invitar);
		dialog.setTitle("Email del usuario a invitar:");
		Button b = (Button) dialog.findViewById(R.id.ok);
		b.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				UsuarioSingleton u=UsuarioSingleton.getInstance();
				InvitarUsuarioAsyncTask tarea = new InvitarUsuarioAsyncTask();
				JSONObject obj= new JSONObject();
				JSONObject aux = new JSONObject();
				try 
				{
					obj.put("email", email.getText().toString());
					aux.put("id", u.getPiso().getId());
					obj.put("idPiso",aux);
					tarea.execute(obj);

				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				dialog.dismiss();
			}
		});
		Button cancelBtn = (Button) dialog.findViewById(R.id.cancel);
		cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
	
}
